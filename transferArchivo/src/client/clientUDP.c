// Cliente UDP

#define MAXLINE 1024
#define SERV_PORT 14000

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void dg_cli(int sockfd, struct sockaddr *pservaddr, long servlen, char mensaje[MAXLINE]){
  int n;
  char recvline[MAXLINE+1];
  sendto(sockfd, mensaje, MAXLINE,0,pservaddr,servlen);
  while(strcmp(recvline, "exit()") != 0){
    n= recvfrom(sockfd, recvline, MAXLINE, 0, NULL, NULL);
    if(strcmp(recvline, "exit()") != 0){
       fputs(recvline, stdout);
    }
  }
}

//Método Principal main
int main(int argc, char **argv){
  int sockfd;
  char mensaje[MAXLINE];
  struct sockaddr_in serveraddr;
  
  if(argc < 4)
  {
    fprintf(stdout, "Uso: cliente <direccion IP> <Usuario> <Token> <orden> <parametros>\n");
    exit(-1);
  }
  strcpy(mensaje,argv[2]);
  strcat(mensaje,",");
  strcat(mensaje,argv[3]);
  strcat(mensaje,",");
  strcat(mensaje,argv[4]);
  if(strcmp(argv[4], "view") == 0 && argc >5){
    strcat(mensaje,",");
    strcat(mensaje,argv[5]);
  }
  bzero(&serveraddr, sizeof(serveraddr));
  serveraddr.sin_family= PF_INET;
  serveraddr.sin_port= htons(SERV_PORT);
  serveraddr.sin_addr.s_addr= inet_addr(argv[1]);
  sockfd=socket(PF_INET,SOCK_DGRAM,0);
  dg_cli(sockfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr), mensaje);
  
  exit(0);
}
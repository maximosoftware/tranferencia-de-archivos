#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define BUFFSIZE 1
#define	ERROR	-1
#define	PUERTO	1100
#define MAXLEN 256

/*Prototipos de función*/
void recibirArchivo(int SocketFD, FILE *file);
void enviarConfirmacion(int SocketFD);
void enviarMD5SUM(int SocketFD, char md5[MAXLEN]);
//void enviarArchivo(int SocketFD, FILE *file, char nameFile[80]);

int main(int argc, char *argv[]){
	struct sockaddr_in stSockAddr;
	int sockLen;
	int Res;
	int SocketFD;
	//int recibido;
	//char buffer[BUFFSIZE];
	char mensaje[MAXLEN];
	//char nombre_archivo[80];
	int totalBytesRcvd;
	int bytesRcvd;
	FILE *archivo;
	if(argc != 5){
		perror("Uso: ip nombre_archivo_remoto usuario token");
		exit(-1);	
	}
	if (SocketFD == ERROR){
		perror("cannot create socket");
		exit(EXIT_FAILURE);
	}

	
	/*Se crea el socket*/
	SocketFD = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    
	/*Se verifica la integridad del socket*/
	if (SocketFD == ERROR){
		perror("cannot create socket");
		exit(EXIT_FAILURE);
	}

	/*Se configura la dirección del socket del cliente*/
	memset(&stSockAddr, 0, sizeof stSockAddr);
 
	stSockAddr.sin_family = AF_INET;
	stSockAddr.sin_port = htons(1100);
	Res = inet_pton(AF_INET, argv[1], &stSockAddr.sin_addr);
 
	sockLen = sizeof(stSockAddr);

	if (0 > Res){
		perror("error: El primer paránmetro no es una familia de direcciónes");
		close(SocketFD);
		exit(EXIT_FAILURE);
	}else if (Res == 0){
		perror("char string (El segundo parmetro no contiene una dirección IP válida");
		close(SocketFD);
		exit(EXIT_FAILURE);
	}

	if (connect(SocketFD, (struct sockaddr *)&stSockAddr, sizeof stSockAddr) == ERROR){
		perror("Error a la hora de conectarse con el servidor");
		close(SocketFD);
		exit(EXIT_FAILURE);
	}

	printf("Se ha conectado con el servidor:%s\n",(char *)inet_ntoa(stSockAddr.sin_addr));
    strcpy(mensaje,argv[2]);
    strcat(mensaje,",");
	strcat(mensaje,argv[3]);
	strcat(mensaje,",");
	strcat(mensaje,argv[4]);
    printf("\nmesage %s\n", mensaje);	
    write(SocketFD,mensaje,sizeof(mensaje));
    
    recibirArchivo(SocketFD, archivo);

	close(SocketFD);

	return 0;
}//End main

void recibirArchivo(int SocketFD, FILE *file){
	char buffer[BUFFSIZE];
	int recibido = -1;
	char message[MAXLEN];
	int readInt;
	readInt=read(SocketFD,message,sizeof(message));
	if(readInt<=0){
		close(SocketFD);
		printf("%s\n", "Error");
		exit(EXIT_FAILURE);
	}
	printf("Nombre de archivo:%s\n",message);
	/*Se abre el archivo para escritura*/
	char ruta[MAXLEN]="./archivos/";
    strcat(ruta,message);
	file = fopen(ruta,"wb");
	if(!file){
		if(mkdir("archivos")!=0){
	    	close(SocketFD);
		    exit(EXIT_FAILURE);
		    perror("Error al abrir el archivo:");
	    }
	    else{
	    	file = fopen(ruta,"wb");
	    }
	
	}
	enviarConfirmacion(SocketFD);
	enviarMD5SUM(SocketFD,message);
    while((recibido = recv(SocketFD, buffer, BUFFSIZE, 0)) > 0){
		//printf("%d\n",recibido);
		fwrite(buffer,sizeof(char),1,file);
	}//Termina la recepción del archivo
	printf("\n%s","Archivo recibido!");
	
	fclose(file);
	

}//End recibirArchivo procedure

void enviarConfirmacion(int SocketFD){
	char mensaje[MAXLEN] = "Paquete Recibido";
	//int lenMensaje = strlen(mensaje);
	printf("\nConfirmación enviada\n");
	if(write(SocketFD,mensaje,sizeof(mensaje)) == ERROR)
			perror("Error al enviar la confirmación:");

	
}//End enviarConfirmacion

void enviarMD5SUM(int SocketFD, char md5[MAXLEN]){
	FILE *fichero=NULL;
	char md5sum[MAXLEN];
	char verify[MAXLEN]="md5sum ";
	strcat(verify,"'archivos/");
	strcat(verify,md5);
	strcat(verify,"'");
	fichero = popen (verify, "r");
	
	fscanf(fichero,"%s",md5sum);	
	printf("MD5SUM :%s\n",md5sum);	
	write(SocketFD,md5sum,sizeof(md5sum));
	fclose(fichero);

}//End enviarMD5DUM
// Cliente UDP

#define MAXLINE 1024
#define SERV_PORT 14000

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <locale.h>

char usuario[30];
char contrasenia[30];
int token;
char ip[30];
char orden[100];
void login(char usuario[30], char contrasenia[30]);
void salir(char usuario[30], int token);
void get(char usuario[30], int token, char file[100]);
void listar(char usuario[30], int token);
void info(char usuario[30], int token, char file[100]);

void login(char usuario[30], char contrasenia[30]){
    FILE *fichero=NULL;
    char *mensaje = NULL;
    char comand [100] = "../rpc/./autenticacion_client ";
    strcat(comand,ip);
    strcat(comand," ");
    strcat(comand,usuario);
    strcat(comand," ");
    strcat(comand,contrasenia);
    strcat(comand," entrar");
    fichero = popen (comand, "r");
    if (fichero == NULL)
    {
      perror ("No se puede abrir:");
      exit (-1);
    }
    while(fscanf(fichero,"%d", &token) != EOF) {
      if(token==0){
        exit(-1);
      }
      else{
        break;
      }
    }
    pclose (fichero);
    while(1){
      char nomfile[100];
      printf("--------------------------------------------------------\nIngrese orden:\nsalir -> si desea salir\nlistar -> si desea listar el directorio remoto\nget nomfile -> para copiar el archivo\nver nomfile -> para ver propiedades fichero\n----------------------------------------------------\n");
      scanf("%s",orden);
      fflush(stdin);
      if(strcmp(orden, "salir") == 0){
        salir(usuario,token);
      }
      else if(strcmp(orden, "listar") == 0){
        listar(usuario,token);
      }
      else if(strcmp(orden, "get") == 0){
        scanf("%s",nomfile);
        //printf("%s\n", nomfile);
        get(usuario,token,nomfile);
      }
      else if(strcmp(orden, "ver") == 0){
        scanf("%s",nomfile);
        //printf("%s\n", nomfile);
        info(usuario,token,nomfile);
      }
      //free(sms);
    }
    
     
}
void salir(char usuario[30], int token){
    FILE *fichero=NULL;
    char *mensaje = NULL;
    char comand [100] = "../rpc/./autenticacion_client ";
    strcat(comand,ip);
    strcat(comand," ");
    strcat(comand,usuario);
    strcat(comand," ");
    char texto[30];
    sprintf(texto, "%d", token);
    strcat(comand,texto);
    strcat(comand," salir");
    fichero = popen (comand, "r");
    if (fichero == NULL)
    {
      perror ("No se puede abrir:");
      exit (-1);
    }
    exit(0);
    pclose (fichero);
}
void get(char usuario[30], int token, char file[100]){
  FILE *fichero=NULL;
  char *mensaje = NULL;
  char comand [100] = "./clientTCPfiles ";
  strcat(comand,ip);
  strcat(comand," ");
  strcat(comand,file);
  strcat(comand," ");
  strcat(comand,usuario);
  strcat(comand," ");
  char texto[30];
  sprintf(texto, "%d", token);
  strcat(comand,texto);
    
  fichero = popen (comand, "r");
  if (fichero == NULL)
  {
    perror ("No se puede abrir:");
    exit (-1);
  }
  size_t leng = 0;
  while (getline(&mensaje, &leng, fichero) != -1) {
    fputs(mensaje, stdout);
  }
  pclose (fichero);
}
void listar(char usuario[30], int token){
    FILE *fichero=NULL;
    char *mensaje = NULL;
    char comand [100] = "./clientUDP ";
    strcat(comand,ip);
    strcat(comand," ");
    strcat(comand,usuario);
    strcat(comand," ");
    char texto[30];
    sprintf(texto, "%d", token);
    strcat(comand,texto);
    strcat(comand," ls");
    fichero = popen (comand, "r");
    if (fichero == NULL)
    {
      perror ("No se puede abrir:");
      exit (-1);
    }
    size_t leng = 0;
    while (getline(&mensaje, &leng, fichero) != -1) {
      fputs(mensaje, stdout);
    }
    pclose (fichero);  
}
void info(char usuario[30], int token, char file[100]){
  FILE *fichero=NULL;
  char *mensaje = NULL;
  char comand [100] = "./clientUDP ";
  strcat(comand,ip);
  strcat(comand," ");
  strcat(comand,usuario);
  strcat(comand," ");
  char texto[30];
  sprintf(texto, "%d", token);
  strcat(comand,texto);
  strcat(comand," view ");
  strcat(comand,file);
  fichero = popen (comand, "r");
  if (fichero == NULL)
  {
    perror ("No se puede abrir:");
    exit (-1);
  }
  size_t leng = 0;
  while (getline(&mensaje, &leng, fichero) != -1) {
    fputs(mensaje, stdout);
  }
  pclose (fichero);  
}
//Método Principal main
int main(int argc, char **argv){
  if(argc!=2){
    perror("Falta de argumentos, host");
    exit(-1);
  }
  strcpy(ip,argv[1]);
  setlocale(LC_ALL, "es_ES");  //Establecemos region para aceptar acentos 
  char usuario[30];
  char contrasenia[30];
  printf("Ingrese Usuario--> ");
  scanf("%s", &usuario);
  fflush(stdin);
  printf("Ingrese contrasenia--> ");
  scanf("%s", &contrasenia);
  fflush(stdin);
  login(usuario, contrasenia);
  return 0;
}
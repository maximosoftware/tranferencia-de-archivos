struct Usuario{
    char usuario[30];
    char pass[30];
};
struct UsuarioL{
    char usuario[30];
    int token;
};
struct square_out
{
	int token;
};

program AUT_PROG{
	version AUT_VER{
        square_out validarUsuario(Usuario)=1;
        square_out logoutUsuario(UsuarioL)=2;
	}= 1;

}= 0x20000001;


#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <netdb.h>


/*Definición de constantes*/
#define BUFFSIZE 1
#define PUERTO		1100
#define ERROR		-1
#define direccIP "0.0.0.0"
#define MAXLEN 256
struct parametros {
	int socket;
};   
/*Prototipos de función*/

void enviarArchivo(int SocketFD, FILE *file, char nameFile[MAXLEN]);
void * socketFun( void * arg );

int main(int argc)
{
    struct sockaddr_in stSockAddr;
    
	
	int SocketServerFD;
	int SocketClientFD;
	int clientLen;
	
	
	/*Se crea el socket*/
	if((SocketServerFD = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) == ERROR){
		perror("No se puede crear el socket");
		exit(EXIT_FAILURE);
	}//End if-SocketFD
 
	/*Se configura la dirección del socket*/
	memset(&stSockAddr, 0, sizeof stSockAddr);
	 
	stSockAddr.sin_family = AF_INET;
	stSockAddr.sin_port = htons(PUERTO);
	stSockAddr.sin_addr.s_addr = INADDR_ANY;
 
	if(bind(SocketServerFD,(struct sockaddr *)&stSockAddr, sizeof stSockAddr) == ERROR){
		perror("Error en bind");
		close(SocketServerFD);
		exit(EXIT_FAILURE);
	}//End if-bind
	inet_pton(AF_INET, direccIP, &stSockAddr.sin_addr);
	printf("Socket atado a la dirección %s\n",(char *)inet_ntoa(stSockAddr.sin_addr));	
	if(listen(SocketServerFD, 10) == ERROR){
		perror("Error listen");
		close(SocketServerFD);
		exit(EXIT_FAILURE);
	}//End if-listen

	while (1){
        struct sockaddr_in clSockAddr;
		clientLen = sizeof(clSockAddr);

		//Espera por la conexión de un cliente//
		if ((SocketClientFD = accept(SocketServerFD, 
						    (struct sockaddr *) &clSockAddr,
						    &clientLen)) < 0){
			perror("Fallo para acpetar la conexión del cliente");
		}//End if-accept

		/*Se configura la dirección del cliente*/
		clSockAddr.sin_family = AF_INET;
		clSockAddr.sin_port = htons(PUERTO);
		pthread_t h1;
		struct parametros par;
		par.socket=SocketClientFD;
        printf("Cliente conectado: %s\n",(char *)inet_ntoa(clSockAddr.sin_addr));
		pthread_create (&h1, NULL, socketFun, (void *)&par);
  		//pthread_join (h1, NULL);
    }
	
 	printf("%s\n", "fin socket");
	close(SocketServerFD);
	return 0;
}//End main program
void * socketFun( void * arg ){
    struct parametros * p;
    p = (struct parametros *) arg;
	char mensaje[MAXLEN];
	char nombre_archivo[MAXLEN];
	char usuario[MAXLEN];
	int token;
	int SocketClientFD;
	SocketClientFD=p->socket;
    char *sms;
	//printf("Cliente conectado: %s\n",(char *)inet_ntoa(clSockAddr.sin_addr));
	read(SocketClientFD,mensaje,sizeof(nombre_archivo));
	sms= strtok(mensaje, ",");
	strcpy(nombre_archivo,sms);
	sms= strtok(NULL, ",");
	strcpy(usuario,sms);
	sms= strtok(NULL, ",");
	token=atoi(sms);
    printf("file %s usuario %s token %d\n", nombre_archivo, usuario,token);
    FILE *archivo;
	/*Se abre el archivo a enviar*/
	

	if(!validarUsuario(usuario,token)){
		perror("Error token incorrecto:");
        char nameFile[MAXLEN];
        strcpy(nameFile, "Error el token especificado no es valido");
        //write(SocketClientFD,nameFile,sizeof(nameFile));
        close(SocketClientFD);
	}
	else{
		char ruta[MAXLEN]="../archivos/";
    	strcat(ruta,nombre_archivo);
		archivo = fopen(ruta,"rb");
		printf("nombre archivo%s\n", nombre_archivo);
		if(!archivo){
		    perror("Error al abrir el archivo:");
		    char nameFile[MAXLEN];
		    strcpy(nameFile, "Error al abrir el archivo");
		    //write(SocketClientFD,nameFile,sizeof(nameFile));
		    close(SocketClientFD);
		}
		else{
			write(SocketClientFD,nombre_archivo,sizeof(nombre_archivo));
			enviarArchivo(SocketClientFD, archivo, nombre_archivo);
			printf("%s\n", "fin ciclo envio");	
			read(SocketClientFD,mensaje,sizeof(mensaje));
			printf("\nConfirmación recibida:\n%s\n",mensaje);
			
			read(SocketClientFD,mensaje,sizeof(mensaje));
			printf("\nMD5SUM:\n%s\n",mensaje);
			fclose(archivo);
			close(SocketClientFD);
		}
	}

}
void enviarArchivo(int SocketFD, FILE *file, char nameFile[MAXLEN]){
	char buffer[BUFFSIZE];
		/*Se envia el archivo*/
		
	printf("Nom file: %d\n",strlen(nameFile));
	
		
	while(!feof(file)){
		fread(buffer,sizeof(char),BUFFSIZE,file);
		if(send(SocketFD,buffer,BUFFSIZE,0) == ERROR){
			perror("Error al enviar el arvhivo:");
			close(SocketFD);
			break;
		}
	}
	//shutdown(SocketFD, 1);
}
int validarUsuario(char usuario[MAXLEN], int token){
	int tokenFile;
    int usrExiste = 0;
    char ruta[MAXLEN]="../usuarios/";
    strcat(ruta,usuario);
    FILE *archivo= fopen(ruta, "r");
    if(archivo != NULL)
    {
        while(fscanf(archivo,"%d", &tokenFile) != EOF) //Leemos archivo
        {   
            if(token == tokenFile)  //Comparamos el token
            {   
                fclose(archivo);
                usrExiste= 1;
                return usrExiste;
            }
        }
    }
    printf("%d\n", usrExiste);
    return usrExiste;
}
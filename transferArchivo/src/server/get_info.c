#include <unistd.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
int main(int argc, char **argv)
{
    if(argc != 2){    
        return 1;
    }
 
    struct stat fileStat;
    if(stat(argv[1],&fileStat) < 0){    
        return 1;
    }
 
    printf("Information for %s\n",argv[1]);
    printf("---------------------------\n");
    printf("File Size: \t\t\t%f Kilobytes\n",(fileStat.st_size/1024.0));
    printf("Number of Links: \t\t%d\n",fileStat.st_nlink);
    printf("File inode: \t\t\t%d\n",fileStat.st_ino);
	printf("File protection: \t\t%o\n",fileStat.st_mode);         /* type + mode */
	printf("File owner id: \t\t\t%d\n", fileStat.st_uid);          /* user id     */
	printf("File group: \t\t\t%d\n", fileStat.st_gid);          /* group id    */
	printf("Blocksize for filesystem I/O: \t%ld\n",  fileStat.st_blksize);
	printf("Number of blocks allocated: \t%ld\n",    fileStat.st_blocks);
	printf("Time of last access: \t\t%s",        ctime(&fileStat.st_atime));
	printf("Time of last modification: \t%s",  ctime(&fileStat.st_mtime));
	printf("Time of last change: \t\t%s",        ctime(&fileStat.st_ctime));
	
	printf("File Permissions: \t\t");
    printf( (S_ISDIR(fileStat.st_mode)) ? "d" : "-");
    printf( (fileStat.st_mode & S_IRUSR) ? "r" : "-");
    printf( (fileStat.st_mode & S_IWUSR) ? "w" : "-");
    printf( (fileStat.st_mode & S_IXUSR) ? "x" : "-");
    printf( (fileStat.st_mode & S_IRGRP) ? "r" : "-");
    printf( (fileStat.st_mode & S_IWGRP) ? "w" : "-");
    printf( (fileStat.st_mode & S_IXGRP) ? "x" : "-");
    printf( (fileStat.st_mode & S_IROTH) ? "r" : "-");
    printf( (fileStat.st_mode & S_IWOTH) ? "w" : "-");
    printf( (fileStat.st_mode & S_IXOTH) ? "x" : "-");
    printf("\n");
	
	/* Fichero con el que abriremos el comando */
	FILE *fichero=NULL;

	/* Variable auxiliar para leer el fichero */
	char aux[1000];

   /* Se abre el fichero del comando*/   
	char comand [60] = "file --mime-type -bp ";
	strcat(comand,"'");
	strcat(comand,argv[1]);
	strcat(comand,"'");
	fichero = popen (comand, "r");
	if (fichero == NULL)
	{
		perror ("No se puede abrir:");
		exit (-1);
	}

	/* Se lee la primera linea y se hace un bucle, hasta fin de fichero,
	 * para ir sacando por pantalla los resultados.
	 */
	fscanf(fichero,"%s",aux);
	printf("Type of File: \t\t\t%s\n",aux);

	/* Se cierra el fichero */
	pclose (fichero);
 
    return 0;
}
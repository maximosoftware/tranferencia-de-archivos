//Servidor

#define MAXLINE 1024
#define SERV_PORT 14000
#define DIRECTORIO "../archivos/"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//#include <unistd.h>

struct parametros {
	int socket;
	long client_lenght;
	struct sockaddr *socket_address;
};
void * dg_echo( void * arg );
int validarUsuario(char usuario[MAXLINE], int token);


int main(int argc, char *argv[]){
  int sockfd;
  pthread_t h1;
  struct sockaddr_in serveraddr, cliaddr;
  struct parametros par;
  
  sockfd= socket(PF_INET, SOCK_DGRAM, 0);
  
  bzero(&serveraddr, sizeof(serveraddr));
  serveraddr.sin_family= PF_INET;
  serveraddr.sin_addr.s_addr= htonl(INADDR_ANY);
  serveraddr.sin_port= htons(SERV_PORT);
  
  bind(sockfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr));
  
  par.socket = sockfd;
  par.client_lenght = sizeof(cliaddr);
  par.socket_address = (struct sockaddr *)&cliaddr;
  
  pthread_create (&h1, NULL, dg_echo, (void *)&par);
  pthread_join (h1, NULL);
}

void * dg_echo( void * arg ){
	struct parametros * p;
	p = (struct parametros *) arg;
		int n;
	socklen_t len;
	char msg[MAXLINE];

	int sockfd;

	struct sockaddr *pcliaddr;
	pcliaddr = (struct sockaddr *) p->socket_address;
	sockfd = p->socket;

	while(1){	
	    len= p->client_lenght;
	    recvfrom(sockfd, msg, MAXLINE, 0, pcliaddr, &len);
		FILE *fichero=NULL;
		char orden[MAXLINE];
		char usuario[MAXLINE];
		char fileInfo[MAXLINE];
		int token;
		char *sms;
		sms= strtok(msg, ",");
		strcpy(usuario,sms);
		sms= strtok(NULL, ",");
		token=atoi(sms);
		sms= strtok(NULL, ",");
		strcpy(orden,sms);
		if(strcmp(orden, "view") == 0){
			sms= strtok(NULL, ",");
			strcpy(fileInfo,sms);
		}
		if(!validarUsuario(usuario,token)){
			perror("Error token incorrecto:");
			sendto(sockfd,  "exit()", MAXLINE,0,pcliaddr,len);
	    }
	    else{
	    	if(strcmp(orden,"view") == 0){
	    		printf("%s\n", "orden ver");
	    		FILE *fichero=NULL;
	    		char *mensaje = NULL;
				char comand [60] = "./get_info ";
				strcat(comand,"'");
				strcat(comand,DIRECTORIO);
				strcat(comand,fileInfo);
				strcat(comand,"'");
				size_t leng = 0;
				fichero = popen (comand, "r");
				if (fichero == NULL)
				{
					perror ("No se puede abrir:");
					exit (-1);
				}
				while (getline(&mensaje, &leng, fichero) != -1) {
					sendto(sockfd,  mensaje, MAXLINE,0,pcliaddr,len);
           		}
				pclose (fichero);
				sendto(sockfd,  "exit()", MAXLINE,0,pcliaddr,len);
	    	}
	    	else if(strcmp(orden, "ls") == 0){
				printf("%s\n", "orden listar");
	    		FILE *fichero=NULL;
	    		char *mensaje = NULL;
				char comand [60] = "./listar ";
				strcat(comand,"'");
				strcat(comand,DIRECTORIO);
				strcat(comand,"'");
				size_t leng = 0;
				fichero = popen (comand, "r");
				if (fichero == NULL)
				{
					perror ("No se puede abrir:");
					exit (-1);
				}
				while (getline(&mensaje, &leng, fichero) != -1) {
					sendto(sockfd,  mensaje, MAXLINE,0,pcliaddr,len);
           		}
				pclose (fichero);
				sendto(sockfd,  "exit()", MAXLINE,0,pcliaddr,len);
	    	}
	    	//printf("%s\n", "usuario valido");
	    	
	  	}
	}
}
int validarUsuario(char usuario[MAXLINE], int token){
	int tokenFile;
    int usrExiste = 0;
    char ruta[MAXLINE]="../usuarios/";
    strcat(ruta,usuario);
    FILE *archivo= fopen(ruta, "r");
    if(archivo != NULL)
    {
        while(fscanf(archivo,"%d", &tokenFile) != EOF) //Leemos archivo
        {   
            if(token == tokenFile)  //Comparamos el token
            {   
                fclose(archivo);
                usrExiste= 1;
                return usrExiste;
            }
        }
    }
    printf("%d\n", usrExiste);
    return usrExiste;
}	

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

/* Lista el contenido de un directorio, pasado como argumento del
 * programa. Basado en funciones POSIX de la biblioteca GNU de C.
 */
int main( int argc, char ** argv ) {
	
	const char * dir_name;
	DIR * dir_ptr = NULL;
	struct dirent * dirent_ptr;
	
	/* comprueba que se haya pasado un único argumento */
	if ( argc != 2 ) {
		printf( "Use list <directorio>\n" );
		return -1;
	}
	
	/* comprueba si directorio existe, y tiene permisos para abrirlo */
	dir_name = argv[1];
	if ( ( dir_ptr = opendir( dir_name ) ) == NULL ) {
		printf( "No existe o no se pudo abrir el directorio '%s'\n", dir_name );
		return -1;
	}
	
	/* ahora listamos el directorio */
	while (( dirent_ptr = readdir( dir_ptr ) ) != NULL ) {
		printf( "%s\n", dirent_ptr -> d_name );
	}
	
	/* cierra el directorio */
	if ( dir_ptr != NULL ) closedir( dir_ptr );

        /* bye .... */
        return 0;
}